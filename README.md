# Ansible Role: Calibre

This role installs [Calibre](https://calibre-ebook.com/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    calibre_home: /opt/calibre
    calibre_arch: x86_64
    calibre_repo_path: https://github.com/kovidgoyal/calibre/releases/download

This role always installs the latest version. See [available Calibre releases](https://github.com/kovidgoyal/calibre/releases/).

    calibre_repo_path: https://github.com/kovidgoyal/calibre/releases/download

The path to the home Calibre directory.

    calibre_home: /opt/calibre

The location where the Calibre binary will be installed.

The path to the home Calibre directory.

    calibre_arch: x86_64

Calibre supports i386 and x86_64 CPU architectures, just change for the main architecture of your CPU

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: calibre
      
      vars_prompt:
        - name: github_token
          prompt: "What is your GitHub Token?"
          default: "{{ lookup('env','GITHUB_TOKEN') }}"
          private: yes

The GitHub Token is optional (until you're restricted for download from the GitHub API), you will be enter to bypassing input your token.
## License

MIT / BSD
